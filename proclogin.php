<html>
	<head>
		<title>Location Goes</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#"><img src="images/logo.png" height="30"></a>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
				<a class="nav-link" href="register.php">Register</a>
		      </li>
		      <li class="nav-item active">
				<a class="nav-link" href="login.php">Login</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="container">
			<div class="col-lg-12">
				<br /><br />
<?php

$dbAccess;
$dbuser = "jfeit_root";
$dbpass = "12345";
$db = "jfeit_demosite";
$dburl = "localhost";
$apikey = "21bnj32kd9xck42";
$url = "http://api.logoauthentication.com/authenticateuser.php";

try {
	$dbAccess = new PDO('mysql:host=' . $dburl . ';dbname=' . $db, $dbuser, $dbpass);
	$dbAccess->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "An error has occured when trying to connect to the database.";
}

$query = $dbAccess->prepare("SELECT hex FROM accounts WHERE username = :username AND password = :password");
$query->execute(array(
	':username' => $_POST['username'],
	':password' => $_POST['password']
));

if($query->rowCount() == 0) {
	echo 'Invalid username or password. <a href="login.php">Back to login</a>';
	return;
}

$userResults = $query->fetch(PDO::FETCH_ASSOC);

echo '<h1>Checking 2FA...</h1><br /><br />';

$result = file_get_contents($url . "?apikey=" . $apikey . "&hex=" . $userResults['hex'] . "&ip=" . $_SERVER['REMOTE_ADDR']);
switch($result)
{
	case '0':
		echo '<div class="alert alert-success"><i class="glyphicon glyphicon-ok"></i>You have successfully logged in</div>';
		break;
	case '1':
		echo '<div class="alert alert-danger"><i class="glyphicon glyphicon-remove"></i>Bad API key!</div>';
		break;
	case '2':
		echo '<div class="alert alert-danger"><i class="glyphicon glyphicon-remove"></i>User has entered a bad hex code!</div>';
		break;
	case '3':
		echo '<div class="alert alert-danger"><i class="glyphicon glyphicon-remove"></i>There was an error trying to geolocate that IP!</div>';
		break;
	case '4':
		echo '<div class="alert alert-danger"><i class="glyphicon glyphicon-remove"></i>There was a problem while trying to get the location of your phone</div>';
		break;
	case '5':
		echo '<div class="alert alert-danger"><i class="glyphicon glyphicon-remove"></i>The location of your phone is not close enough to your IP!</div>';
		break;
	default:
		echo $result;
}

?>
</div>
</div>
</body>
</html>