<html>
	<head>
		<title>Location Goes</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#"><img src="images/logo.png" height="30"></a>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
				<a class="nav-link" href="register.php">Register</a>
		      </li>
		      <li class="nav-item active">
				<a class="nav-link" href="login.php">Login</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="container" style="padding: 20px;">
			<form class="form-signin text-center" style="width: 500px;border: 1px solid #eee;margin: auto;padding: 20px;" action="proclogin.php" method="post">
		      <img class="mb-4" src="images/logo.png" alt="" width="250">
		      <h1 class="h3 mb-3 font-weight-normal">Login</h1>
		      <label for="inputUsername" class="sr-only">Username</label>
		      <input type="text" id="inputUsername" class="form-control" name="username" placeholder="Username" required autofocus>
		      <label for="inputPassword" class="sr-only">Password</label>
		      <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
		      <br />
		      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
		      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
		    </form>
		</div>
	</body>
</html>