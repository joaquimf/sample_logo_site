<?php

$dbAccess;
$dbuser = "root";
$dbpass = "";
$db = "demosite";
$dburl = "localhost";

try {
	$dbAccess = new PDO('mysql:host=' . $dburl . ';dbname=' . $db, $dbuser, $dbpass);
	$dbAccess->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "An error has occured when trying to connect to the database.";
}

$query = $dbAccess->prepare("SELECT id FROM accounts WHERE username = :username");
$query->execute(array(
	':username' => $_POST['username']
));

if($query->rowCount() > 0) {
	echo 'That username is taken! <a href="register.php">Back to register page</a>';
	return;
}

$query = $dbAccess->prepare("INSERT INTO accounts (username, password, hex) VALUES (:username, :password, :hex)");
$query->execute(array(
	':username' => $_POST['username'],
	':password' => $_POST['password'],
	':hex' => $_POST['hex'],
));

if(!$query) {
	echo 'An error has occured. <a href="register.php">Back to register page</a>';
	return;
}