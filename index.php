<html>
	<head>
		<title>Location Goes</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="main.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
		  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="#"><img src="images/logo.png" height="30"></a>
		  <div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
				<a class="nav-link" href="register.php">Register</a>
		      </li>
		      <li class="nav-item">
				<a class="nav-link" href="login.php">Login</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="content-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<h1 class="display-3">Location Go</h1><br />
						<h3>Two-Factor Authentication made easy</h3>
					</div>
					<div class="col-lg-4" style="height: 200px;">
						<img src="images/intro.png" width="100%">
					</div
				</div>
				<div class="row">
					<div class="col-lg-8">
						<p>
							Two-Factor authentication is one of the newest methods of security for account management on the web. Although it is very secure, it is a process to have to take out your phone, type in your code, then have the website verify. With our approach, all you have to do is open your notification after signing into a website, and thats it!. LoGo will do the rest.
						</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>